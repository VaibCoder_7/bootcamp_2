import unittest
import Interval

class TestInterval(unittest.TestCase):
    def setUp(self):
        self.interval0 = Interval.Interval()
        self.interval1 = Interval.Interval(5)
        self.interval2 = Interval.Interval(-2, 6)

    def test_length(self):
        """
        len([0, 0]) = 1
        len([0, 5]) = 6
        len([-2, 6]) = 9
        """
        self.assertEqual(len(self.interval0), 1)
        self.assertEqual(len(self.interval1), 6)
        self.assertEqual(len(self.interval2), 9)

    def test_reset(self):
        self.interval0.reset()
        self.assertTrue(self.interval0.start == 0 and self.interval0.end == 0)

        self.interval1.reset()
        self.assertTrue(self.interval1.start == 0 and self.interval1.end == 0)
        
        self.interval2.reset()
        self.assertTrue(self.interval2.start == 0 and self.interval2.end == 0)

    def test__repr__(self):
        self.assertEqual(self.interval0.__repr__(), "[0]")
        self.assertEqual(self.interval1.__repr__(), "[0, 5]")
        self.assertEqual(self.interval2.__repr__(), "[-2, 6]")

    def test_stretching(self):
        def check_stretch(interval, stretch_by, direction = "both"):
            init_start, init_end = interval.start, interval.end
            interval.stretching(stretch_by, direction)
            if direction == "left":
                self.assertTrue(interval.start == init_start - stretch_by, interval.end == init_end)
            elif direction == "right":
                self.assertTrue(interval.start == init_start, interval.end == init_end + stretch_by)
            elif direction == "both":
                self.assertTrue(interval.start == init_start - stretch_by, interval.end == init_end + stretch_by)


        check_stretch(self.interval0, 5, "left")
        check_stretch(self.interval0, 6, "right")
        check_stretch(self.interval0, 10)

        check_stretch(self.interval2, 5, "left")
        check_stretch(self.interval2, 6, "right")
        check_stretch(self.interval2, 10)

        # check_stretch(self.interval2, -9, "left")
        # check_stretch(self.interval2, -8, "right")
        # check_stretch(self.interval2, -10)

    def test_contains(self):
        """
        Intermediate values test
        """
        self.assertFalse(self.interval0.contains(3))
        self.assertFalse(self.interval1.contains(-1))
        self.assertTrue(self.interval2.contains(0))

        """
        Ends test
        """
        self.assertTrue(self.interval1.contains(0) and self.interval1.contains(5))
        self.assertTrue(self.interval2.contains(-2) and self.interval2.contains(6))

    def test_is_sub_interval(self):
        self.assertTrue(self.interval1.is_sub_interval(self.interval0) and self.interval2.is_sub_interval(self.interval0))
        self.assertTrue(self.interval2.is_sub_interval(self.interval1))
        self.assertFalse(self.interval1.is_sub_interval(self.interval2))

    def test_is_disjoint(self):
        self.assertFalse(self.interval1.is_disjoint(self.interval2))
        self.assertFalse(self.interval2.is_disjoint(self.interval1))
        self.assertFalse(self.interval2.is_disjoint(self.interval0))
        self.assertTrue(self.interval2.is_disjoint(Interval.Interval(23, 45)))
        self.assertTrue(self.interval1.is_disjoint(Interval.Interval(-23, -16)))

    def test_is_overlapping(self):
        self.assertTrue(self.interval1.is_disjoint(self.interval2))
        self.assertTrue(self.interval2.is_disjoint(self.interval1))
        self.assertTrue(self.interval2.is_disjoint(self.interval0))
        self.assertFalse(self.interval2.is_disjoint(Interval.Interval(23, 45)))
        self.assertFalse(self.interval1.is_disjoint(Interval.Interval(-23, -16)))
        
    

    def tearDown(self):
        self.interval0.reset()
        self.interval1.start, self.interval1.end = 0, 5
        self.interval2.start, self.interval2.end = -2, 6
       

if __name__ == "__main__":
    unittest.main()
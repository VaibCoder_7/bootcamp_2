import java.util.ArrayList;

public class Range {
    private int start;
    private int end;

    public Range() {
        this.start = 0;
        this.end = 0;
    }

    public Range(int a) {
        this.start = 0;
        this.end = a;
    }

    public Range(int a, int b) {
        this.start = a;
        this.end = b;
    }

    public Range(Range rangeObj) {
        this.start = rangeObj.start;
        this.end = rangeObj.end;
    }

    public boolean isOverlap(Range rng) {
        return rng.start < this.end;
    }

    public boolean contains(Range rng) {
        return this.start <= rng.start && rng.end < this.end;
    }

    public boolean contains(int a) {
        return this.start <= a && a < this.end;
    }

    public boolean isSubsetOf(Range rng) {
        return rng.start <= this.start && this.end < rng.end;
    }

    public boolean touching(Range rng) {
        return this.end - 1 == rng.start || rng.end - 1 == this.start;
    }

    public boolean hasNegatives() {
        return this.start < 0 || this.end < 0;
    }

    public boolean lessThan(Range rng) {
        if (this.start == rng.start) {
            return this.end - this.start < rng.end - rng.start;
        }
        return this.start < rng.start;
    }

    public void modify(int a) {
        this.end = a;
    }

    public void modify(int a, int b) {
        this.start = a;
        this.end = b;
    }

    public int sum() {
        int rangeSum = 0;

        for (int i = this.start; i < this.end; i ++) {
            rangeSum += i;
        }

        return rangeSum;
    }

    public Range add(Range rng) {
        if (this.isOverlap(rng)) {
            return new Range();
        }
        if (rng.contains(this)) {
            return new Range(rng);
        }
        if (this.contains(rng)) {
            return new Range(this);
        }
        if (this.start < rng.start) {
            return new Range(this.start, rng.end);
        }
        return new Range(rng.start, this.end);
    }

    public ArrayList<Integer> getSet() {
        ArrayList<Integer> range = new ArrayList<Integer>();
        
        for (int i = this.start; i < this.end; i ++) {
            range.add(i);
        }

        return range;
    }

    public String print() {
        return "[" + Integer.toString(this.start) + ", " + Integer.toString(this.end) + ")";
    }
}
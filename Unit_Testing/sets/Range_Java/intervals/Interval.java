package intervals;

import java.util.ArrayList;
import java.util.Collections;

public abstract class Interval {
	protected int low;
	protected int high;
	
	public int getLow() {
		return this.low;
	}
	
	public int getHigh() {
		return this.high;
	}
	
	public enum Relation {
        SUBSET, SUPERSET, OVERLAPL, OVERLAPR, TOUCHINGL, 
        TOUCHINGR, LESSDISJOINT, MOREDISJOINT, SAME;
    }
	public enum DIRECTION {
		RIGHT, LEFT, BOTH;
    }
	
	public Interval() {
		this.low = this.high = 0;
	}
	
	public Interval(int end) throws Exception {
		if (end == 0) {
			throw new Exception("Low and high ends of the interval cannot be the same!");
		}
		this.low = Math.min(0, end);
		this.high = end < 0 ? Integer.MAX_VALUE : end;
	}
	
	public Interval(int start, int end) throws Exception {
		if (start == end) {
			throw new Exception("Low and high ends of the interval cannot be the same!");
		}
		this.low = start;
		this.high = end;
	}
	
	private void reset() {
		this.low = this.high = 0;
	}
	
	public void stretch(DIRECTION direction) {
		if (direction == DIRECTION.LEFT) {
			this.low --;
		}
		else if (direction == DIRECTION.RIGHT) {
			this.high ++;
		}
		else if (direction == DIRECTION.BOTH) {
			this.low --;
			this.high ++;
		}
		if (this.low >= this.high) {
			this.reset();
		}
	}
	
	public void stretch(int by, DIRECTION direction) {
		if (direction == DIRECTION.LEFT) {
			this.low -= by;
		}
		else if (direction == DIRECTION.RIGHT) {
			this.high += by;
		}
		else if (direction == DIRECTION.BOTH) {
			this.low -= by;
			this.high += by;
		}
		
		if (this.low >= this.high) {
			this.reset();
		}
	}
	
	public void squeeze(DIRECTION direction) {
		this.stretch(-1, direction);
	}
	
	public void squeeze(int by, DIRECTION direction) {
		this.stretch(-by, direction);
	}
	
	public boolean equals(Interval i) {
		return i.low == this.low && i.high == this.high;
	}
	
	public boolean isOverlapping(Interval i) {
		return !this.isDisjoint(i);
	}
	
	public boolean lessThan(Interval i) {
		return this.low < i.low;
	}
	
	public String toString() {
		return this.low + ", " + this.high;
	}
	
	public Closed merge(Interval i) throws Exception {
		/* 
		 * This method merges the two intervals in the 
		 * a closed interval.
		 */
		
		if (this.isDisjoint(i)) {
//			throw new Exception("Cannot add the intervals as they are disjoint!");
			return new Closed();
		}
		int low = Math.min(this.low, i.low);
		int high = Math.max(this.high, i.high);
		
		return new Closed(low, high);
	}
	
	public Closed common(Interval i) throws Exception {
		/* 
		 * This method stores the common of the two intervals 
		 * in a closed interval.
		 */
		
		if (this.isDisjoint(i)) {
			return new Closed();
		}
		
		ArrayList<Integer> limits = new ArrayList<Integer>();
		limits.add(this.low);
		limits.add(i.low);
		limits.add(this.high);
		limits.add(i.high);
		
		Collections.sort(limits);
		
		return new Closed(limits.get(1), limits.get(2));
	}
	
	public abstract int length();
	public abstract boolean contains(int x);
	public abstract boolean contains(Interval i);
	public abstract boolean isDisjoint(Interval i);
	public abstract Relation classify(Interval i);
}

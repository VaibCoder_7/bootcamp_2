/**
 * 
 */
package intervals;


/**
 * @author Vaibhavi
 *
 */
public class Open extends Interval {

	/**
	 * 
	 */
	public Open() {
		super();
	}

	/**
	 * @param end
	 * @throws Exception 
	 */
	public Open(int end) throws Exception {
		super(end);
	}

	/**
	 * @param start
	 * @param end
	 * @throws Exception 
	 */
	public Open(int start, int end) throws Exception {
		super(start, end);
	}

	@Override
	public int length() {
		return this.high - this.low - 1;
	}

	@Override
	public boolean contains(int x) {
		return this.low < x && x < this.high;
	}

	@Override
	public boolean contains(Interval i) {
		if (!i.contains(i.low) && !i.contains(i.high))
			return this.low <= i.low && i.high <= this.high;
		if (i.contains(i.low) && !i.contains(i.high))
			return this.low <= i.low && i.high < this.high;
		return this.low < i.low && i.high < this.high;
	}

	@Override
	public boolean isDisjoint(Interval i) {
		return i.high <= this.low || this.high <= i.low ;
	}

	@Override
	public Relation classify(Interval i) {
		if (this.high - 1 == i.low + 1) 
            return Relation.TOUCHINGR;
        if (this.low + 1 == i.high - 1)
            return Relation.TOUCHINGL;
        if (this.equals(i))
            return Relation.SAME;
        if (this.contains(i)) 
            return Relation.SUPERSET;
        if (i.contains(this))
            return Relation.SUBSET;
        if (this.isDisjoint(i))
            if (this.low > i.high)
                return Relation.MOREDISJOINT;
            else
                return Relation.LESSDISJOINT;
        if (this.lessThan(i))
            return Relation.OVERLAPL;
        return Relation.OVERLAPR;
	}

	@Override
	public String toString() {
		return "(" + super.toString() + ")";
	}
}

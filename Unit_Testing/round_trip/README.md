# Round Trip Determiner

## Problem Statement
Given a file with 4 columns: date, person, from place, to place; determine did a how many of the passengers completed a round trip.

## Assumptions
For the first iteration, we have:
1. One person has only one round trip.
2. There are no 2 persons having the same name.
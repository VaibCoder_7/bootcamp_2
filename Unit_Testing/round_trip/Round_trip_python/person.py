class Person():
    def __init__(self, start):
        self.start_loc = start
        self.round_completed = False

    def update_completion_status(self, end):
        if not self.round_completed:
            self.round_completed = self.start_loc == end
